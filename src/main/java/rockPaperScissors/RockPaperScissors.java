package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    public static String randomChoices(){
        String [] choice = {"rock", "paper", "scissors"};
        Random rand = new Random();
        int computerChoice = rand.nextInt(3);
        return choice[computerChoice];
    }


    public String userChoice(){
        String choice = readInput("Your choice (Rock/Paper/Scissors)?");

        while (!choice.equals("rock") && !choice.equals("paper") && !choice.equals("scissors")){
            choice = readInput("I don't understand. Try again");
        }

        return choice.toLowerCase();
    }

    public boolean continuePlaying() {
        boolean continuePlaying = false;
        String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
        if (continueAnswer.equals("y")) {
            continuePlaying = true;
            roundCounter++;
        }else if(!continueAnswer.equals("y")) {
            System.out.println("Bye bye :)");
            return continuePlaying;
        }
        return continuePlaying;
    }

    public void determineWinner(String computer, String user) {
        if (computer.equalsIgnoreCase("rock") && user.equalsIgnoreCase("paper")) {
            System.out.println(" User wins!");
            humanScore++;
        } else if (computer.equalsIgnoreCase("rock") && user.equalsIgnoreCase("scissors")) {
            System.out.println(" Computer wins!");
            computerScore++;
        } else if (computer.equalsIgnoreCase("paper") && user.equalsIgnoreCase("rock")) {
            System.out.println(" Computer wins!");
            computerScore++;
        } else if (computer.equalsIgnoreCase("paper") && user.equalsIgnoreCase("scissors")) {
            System.out.println(" User wins!");
            humanScore++;
        } else if (computer.equalsIgnoreCase("scissors") && user.equalsIgnoreCase("rock")) {
            System.out.println(" User wins!");
            humanScore++;
        } else if (computer.equalsIgnoreCase("scissors") && user.equalsIgnoreCase("paper")) {
            System.out.println(" Computer wins!");
            computerScore++;
        } else if (computer.equalsIgnoreCase(user)) {
            System.out.println(" It's a tie!");
        }
    }

    public void run() {
        String computer, user;

        while(true) {
            System.out.println("Let's play round " + roundCounter);
            computer = randomChoices();
            user = userChoice();
            System.out.print("Human chose " + user + ", computer chose " + computer + ".");

            determineWinner(computer, user);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            if(!continuePlaying()){
                break;
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput (String prompt){
        System.out.println(prompt);
        return sc.next().toLowerCase();
    }
}
